package org.bitbucket.sergey_kozlov.playingcards.deck52

/**
 * User: skozlov
 * E-mail: mail.sergey.kozlov@gmail.com
 * Date: 08.06.2014
 */
object Rank extends Enumeration{
	type Rank = Value

	val A = Value("A")
	val K = Value("K")
	val Q = Value("Q")
	val J = Value("J")
	val T = Value("T")
	val _9 = Value("9")
	val _8 = Value("8")
	val _7 = Value("7")
	val _6 = Value("6")
	val _5 = Value("5")
	val _4 = Value("4")
	val _3 = Value("3")
	val _2 = Value("2")
}