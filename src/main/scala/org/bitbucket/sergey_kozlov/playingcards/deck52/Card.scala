package org.bitbucket.sergey_kozlov.playingcards.deck52

import java.util.Objects

import org.bitbucket.sergey_kozlov.playingcards.core.Suit.Suit
import org.bitbucket.sergey_kozlov.playingcards.deck52.Rank.Rank

/**
 * User: skozlov
 * E-mail: mail.sergey.kozlov@gmail.com
 * Date: 08.06.2014
 */
class Card protected(val rank: Rank, val suit: Suit) extends Equals{
	require(rank != null)
	require(suit != null)

	override lazy val toString: String = rank.toString + suit.toString

	override def equals(obj: scala.Any): Boolean = obj match {
		case null => false
		case that: Card => (that canEqual this) && this.rank == that.rank && this.suit == that.suit
		case _ => false
	}

	override def canEqual(that: Any): Boolean = that.isInstanceOf[Card]

	override lazy val hashCode: Int = Objects.hash(rank, suit)
}

object Card{
	def apply(rank: Rank, suit: Suit): Card = new Card(rank, suit)

	def unapply(card: Card): Option[(Rank, Suit)] = if(card == null) None else Some(card.rank, card.suit)
}