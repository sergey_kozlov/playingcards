package org.bitbucket.sergey_kozlov.playingcards.core

/**
 * User: skozlov
 * E-mail: mail.sergey.kozlov@gmail.com
 * Date: 08.06.2014
 */
object Suit extends Enumeration{
	type Suit = Value

	val Hearts = Value(String.valueOf('\u2665'))
	val Diamonds = Value(String.valueOf('\u2666'))
	val Clubs = Value(String.valueOf('\u2663'))
	val Spades = Value(String.valueOf('\u2660'))
}
