package org.bitbucket.sergey_kozlov.playingcards.holdem

import org.bitbucket.sergey_kozlov.commons.ordered.priority.Priority
import org.bitbucket.sergey_kozlov.playingcards.core.Suit.Suit
import org.bitbucket.sergey_kozlov.playingcards.deck52.Card
import org.bitbucket.sergey_kozlov.playingcards.holdem.HoldemRank.holdemRankToRank52

/**
 * User: Sergey Kozlov skozlov@poidem.ru
 * Date: 27.06.2014
 * Time: 9:49
 */

class HoldemCard protected(val holdemRank: HoldemRank, suit: Suit) extends Card(holdemRank, suit) with Ordered[HoldemCard] with Priority{
	require(holdemRank != null)
	require(suit != null)

	override def canEqual(that: Any): Boolean = that.isInstanceOf[HoldemCard]

	override def compare(that: HoldemCard): Int = {
		if(that == null) 1
		else this.holdemRank compare that.holdemRank
	}

	override lazy val prioritiesCount: Int = holdemRank.prioritiesCount

	override lazy val normalizedPriority: Int = holdemRank.normalizedPriority
}

object HoldemCard{
	def apply(rank: HoldemRank, suit: Suit): HoldemCard = new HoldemCard(rank, suit)

	def unapply(card: HoldemCard): Option[(HoldemRank, Suit)] = if(card == null) None else Some(card.holdemRank, card.suit)
}