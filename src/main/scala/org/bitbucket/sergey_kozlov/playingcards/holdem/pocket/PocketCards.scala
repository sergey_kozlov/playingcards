package org.bitbucket.sergey_kozlov.playingcards.holdem.pocket

import org.bitbucket.sergey_kozlov.playingcards.holdem.HoldemCard
import org.bitbucket.sergey_kozlov.playingcards.holdem.HoldemRank.rank52ToHoldemRank
import org.bitbucket.sergey_kozlov.playingcards.deck52.Rank._

/**
 * User: Sergey Kozlov skozlov@poidem.ru
 * Date: 12.08.2014
 * Time: 10:47
 */

case class PocketCards(high: HoldemCard, low: HoldemCard){
	require(high != low)
	require(high >= low)

	val pair = high.holdemRank == low.holdemRank

	val highPair = pair && high.holdemRank >= Q
}