package org.bitbucket.sergey_kozlov.playingcards.holdem.pocket

import org.bitbucket.sergey_kozlov.playingcards.core.Suit.Suit
import org.bitbucket.sergey_kozlov.playingcards.holdem.{HoldemCard, HoldemRank}

/**
 * User: skozlov
 * E-mail: mail.sergey.kozlov@gmail.com
 * Date: 19.07.2014
 */
case class PocketCard(override val holdemRank: HoldemRank, override val suit: Suit, shown: Boolean = false) extends HoldemCard(holdemRank, suit){
	lazy val show = if(shown) this else PocketCard(rank, suit, shown = true)
}