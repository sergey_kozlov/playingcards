package org.bitbucket.sergey_kozlov.playingcards.holdem

/**
 * User: Sergey Kozlov skozlov@poidem.ru
 * Date: 12.08.2014
 * Time: 10:01
 */

sealed trait Move

case object Fold extends Move

case object Check extends Move

case class Call(size: Int) extends Move{
	require(size > 0)
}

case class Raise(callSize: Int, raised: Int) extends Move{
	require(callSize > 0)
	require(raised > 0)
}