package org.bitbucket.sergey_kozlov.playingcards.holdem.pocket

import org.bitbucket.sergey_kozlov.playingcards.holdem.HoldemRank

/**
 * User: Sergey Kozlov skozlov@poidem.ru
 * Date: 12.08.2014
 * Time: 10:51
 */

object PocketPair{
	def unapply(cards: PocketCards): Option[HoldemRank] = if(cards == null || !cards.pair) None else Some(cards.high.holdemRank)
}