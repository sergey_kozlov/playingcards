package org.bitbucket.sergey_kozlov.playingcards.holdem

import org.bitbucket.sergey_kozlov.commons.ordered.priority.Priority
import org.bitbucket.sergey_kozlov.playingcards.deck52.Rank
import org.bitbucket.sergey_kozlov.playingcards.deck52.Rank._
import org.bitbucket.sergey_kozlov.playingcards.holdem.HoldemRank._

/**
 * User: Sergey Kozlov skozlov@poidem.ru
 * Date: 11.06.2014
 * Time: 15:18
 */

class HoldemRank protected(private val rank: Rank) extends Equals with Priority with Ordered[HoldemRank]{
	require(rank != null)

	override lazy val hashCode: Int = rank.hashCode()

	override def equals(other: Any): Boolean = other match {
		case null => false
		case that: HoldemRank => (that canEqual this) && rank == that.rank
		case _ => false
	}

	override def canEqual(that: Any): Boolean = that.isInstanceOf[HoldemRank]

	override lazy val toString: String = rank.toString

	def +(priorityChanging: Int): HoldemRank = {
		val rank = OrderedRanks((priority + priorityChanging) % Priorities.size)
		new HoldemRank(rank)
	}

	def -(priorityChanging: Int): HoldemRank = this + (-priorityChanging)

	override val prioritiesCount: Int = Priorities.size

	override val normalizedPriority: Int = Priorities(rank)

	override def compare(that: HoldemRank): Int = if(that == null) 1 else this.priority compareTo that.priority
}

object HoldemRank{
	private lazy val OrderedRanks = List(_2, _3, _4, _5, _6, _7, _8, _9, T, J, Q, K, A)
	private lazy val Priorities = (OrderedRanks zip
			(0 to Rank.values.size - 1)).toMap
	lazy val Values: List[HoldemRank] = Rank.values.toList map rank52ToHoldemRank

	implicit def rank52ToHoldemRank(rank: Rank): HoldemRank = new HoldemRank(rank)

	implicit def holdemRankToRank52(rank: HoldemRank) = rank.rank
}