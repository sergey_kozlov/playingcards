package org.bitbucket.sergey_kozlov.playingcards.holdem

import org.bitbucket.sergey_kozlov.commons.compare._
import org.bitbucket.sergey_kozlov.commons.constraint._
import org.bitbucket.sergey_kozlov.commons.ordered.priority.{PrioritiesSeq, Priority}
import org.bitbucket.sergey_kozlov.playingcards.core.Suit.Suit
import org.bitbucket.sergey_kozlov.playingcards.deck52.Rank
import org.bitbucket.sergey_kozlov.playingcards.deck52.Rank.{A, _2, _5}
import org.bitbucket.sergey_kozlov.playingcards.holdem.HoldemRank.rank52ToHoldemRank
import org.bitbucket.sergey_kozlov.math.arithmetic.ExtendedDouble.intToExtendedDouble

/**
 * User: skozlov
 * E-mail: mail.sergey.kozlov@gmail.com
 * Date: 16.06.2014
 */
sealed trait HoldemHand extends Ordered[HoldemHand] with Equals with Priority {
	override def compare(that: HoldemHand): Int = if(that == null) 1 else this.priority compareTo that.priority
}

object HoldemHand{
	private val MinSize = 5
	private val MaxSize = 7
	private lazy val AllowableSizes = MinSize to MaxSize

	def apply(cards: Set[HoldemCard]): HoldemHand = {
		def getStraightLike(cards: List[HoldemCard]): List[HoldemCard] = {
			def find(cards: List[HoldemCard]): (List[HoldemCard], List[HoldemCard]) = {
				if(cards.size <= 1) (cards, cards)
				else{
					val tailResult = find(cards.tail)
					val currentSequence = if(cards.head.holdemRank == tailResult._2.head.holdemRank + 1) cards.head :: tailResult._2 else List(cards.head)
					val maxSequence = if(currentSequence.size >= tailResult._1.size) currentSequence else tailResult._1
					val straightLike = if(cards.head.rank == A && maxSequence.size >= 4 && maxSequence.last.rank == _2) maxSequence :+ cards.head else maxSequence
					(straightLike, currentSequence)
				}
			}

			if(cards.size < 5) Nil else find(cards)._1
		}

		require(cards != null)
		require(AllowableSizes contains cards.size)
		cards foreach(c => require(c != null))

		val sortedCards = cards.toList.sortWith((c1, c2) => c1 >= c2)

		val biggestSuitedGroup: List[HoldemCard] = {
			val groups: Map[Suit, List[HoldemCard]] = sortedCards.groupBy(_.suit)
			groups.maxBy(_._2.size)._2
		}

		if(biggestSuitedGroup.size >= 5){
			val straightLike = getStraightLike(biggestSuitedGroup)
			if(straightLike.size >= 5){
				if(straightLike.head.rank == A) RoyalFlush
				else StraightFlush(straightLike.head.holdemRank)
			}
			else Flush((biggestSuitedGroup take 5) map(_.holdemRank))
		}
		else {
			val straightLike = getStraightLike(sortedCards)
			if(straightLike.size >= 5) Straight(straightLike.head.holdemRank)
			else {
				val rankGroups: List[(HoldemRank, Int)] = {
					val ranks = sortedCards map(_.holdemRank)
					val listGroups: Map[HoldemRank, List[HoldemRank]] = ranks.groupBy(r => r)
					listGroups.map(g => (g._1, g._2.size)).toList.sortWith((g1, g2) => {
						if(g1._2 == g2._2) g1._1 > g2._1
						else g1._2 > g2._2
					})
				}

				rankGroups.head._2 match {
					case 4 =>
						val otherRank: HoldemRank = rankGroups.tail.maxBy(_._1)._1
						val quadsRank: HoldemRank = rankGroups.head._1
						Quads(quadsRank, otherRank)
					case 3 =>
						val threeRank = rankGroups.head._1
						val pairCandidates = rankGroups.tail.takeWhile(_._2 >= 2)
						if(pairCandidates.isEmpty) {
							val otherRanks = ((rankGroups.tail sortWith ((g1, g2) => g1._1 > g2._1)) take 2) map(_._1)
							Three(threeRank, otherRanks)
						}
						else {
							val pairRank = pairCandidates.maxBy(_._1)._1
							FullHouse(threeRank, pairRank)
						}
					case 2 =>
						val pair1Rank = rankGroups.head._1
						val pair2Candidates = rankGroups.tail.takeWhile(_._2 >= 2)
						if(pair2Candidates.isEmpty){
							val otherRanks = ((rankGroups.tail sortWith ((g1, g2) => g1._1 > g2._1)) take 3) map(_._1)
							Pair(pair1Rank, otherRanks)
						}
						else {
							val pair2Rank = pair2Candidates.maxBy(_._1)._1
							val otherRank: HoldemRank = rankGroups.slice(2, rankGroups.size).maxBy(_._1)._1
							TwoPairs(pair1Rank, pair2Rank, otherRank)
						}
					case 1 => HighCard((rankGroups take 5) map (_._1))
				}
			}
		}
	}
}

case object RoyalFlush extends HoldemHand{
	override def compare(that: HoldemHand): Int = if(that == this) 0 else 1

	override lazy val toString: String = "Royal flush"

	override val prioritiesCount: Int = 1

	override val minPriority: Int = StraightFlush.MaxPriority + 1

	override val normalizedPriority: Int = 0
}

case class StraightFlush(highRank: HoldemRank) extends HoldemHand {
	require(highRank != rank52ToHoldemRank(Rank.A), "This is a royal flush")
	require(highRank >= _5)

	override def compare(that: HoldemHand): Int = that match {
		case null => 1
		case RoyalFlush => -1
		case that: StraightFlush => this.highRank compare that.highRank
		case _ => 1
	}

	override lazy val toString: String = "Straight flush from " + highRank

	override val normalizedPriority: Int = highRank.normalizedPriority

	override val minPriority: Int = Quads.MaxPriority + 1

	override val prioritiesCount: Int = StraightFlush.MaxPriority - minPriority + 1
}

object StraightFlush{
	val MaxPriority: Int = Quads.MaxPriority + Rank.values.size
}

case class Quads(rank: HoldemRank, otherRank: HoldemRank) extends HoldemHand with PrioritiesSeq{
	require(rank != null)
	require(otherRank != null)
	require(rank != otherRank, "5 cards of the same rank")

	override def compare(that: HoldemHand): Int = that match{
		case null => 1
		case RoyalFlush => -1
		case _: StraightFlush => -1
		case that: Quads =>
			val ranksComparisonResult = this.rank compare that.rank
			if(ranksComparisonResult != 0) ranksComparisonResult
			else this.otherRank compare that.otherRank
		case _ => 1
	}

	override lazy val toString: String = s"Quads of $rank with $otherRank"

	override val minPriority: Int = FullHouse.MaxPriority + 1

	override protected lazy val priorities: Seq[Priority] = List(rank, otherRank)
}

object Quads{
	val MaxPriority: Int = FullHouse.MaxPriority + (Rank.values.size pow 2).toInt
}

case class FullHouse(threeRank: HoldemRank, pairRank: HoldemRank) extends HoldemHand with PrioritiesSeq{
	require(threeRank != null)
	require(pairRank != null)
	requireDifferent(threeRank, pairRank)

	override def compare(that: HoldemHand): Int = that match{
		case null => 1
		case RoyalFlush => -1
		case _: StraightFlush => -1
		case _: Quads => -1
		case that: FullHouse =>
			val threeComparisonResult = this.threeRank compare that.threeRank
			if(threeComparisonResult != 0) threeComparisonResult
			else this.pairRank compare that.pairRank
		case _ => 1
	}

	override lazy val toString: String = s"Full house (three of $threeRank and pair of $pairRank)"

	override val minPriority: Int = Flush.MaxPriority + 1

	override protected lazy val priorities: Seq[Priority] = List(threeRank, pairRank)
}

object FullHouse{
	val MaxPriority: Int = Flush.MaxPriority + (Rank.values.size pow 2).toInt
}

case class Flush(ranks: Seq[HoldemRank]) extends HoldemHand with PrioritiesSeq{
	require(ranks != null)
	require(ranks.size == 5)
	ranks foreach (r => require(r != null))
	requireDescendingDistinct(ranks:_*)
	require(ranks.head - 4 != ranks.last, "It's not a flush, but a straight flush or a royal flush")

	override def compare(that: HoldemHand): Int = that match{
		case null => 1
		case RoyalFlush => -1
		case _: StraightFlush => -1
		case _: Quads => -1
		case _: FullHouse => -1
		case that: Flush => compareLexicographically(this.ranks, that.ranks)
		case _ => 1
	}

	override lazy val toString: String = "Flush " + ranks.mkString(" ")

	override val minPriority: Int = Straight.MaxPriority + 1

	override protected val priorities: Seq[Priority] = ranks
}

object Flush{
	val MaxPriority: Int = Straight.MaxPriority + (Rank.values.size pow 5).toInt

	def apply(r1: HoldemRank, r2: HoldemRank, r3: HoldemRank, r4: HoldemRank, r5: HoldemRank): Flush = Flush(Seq(r1, r2, r3, r4, r5))
}

case class Straight(highRank: HoldemRank) extends HoldemHand {
	require(highRank >= _5)

	override def compare(that: HoldemHand): Int = that match {
		case null => 1
		case _: HighCard => 1
		case _: Pair => 1
		case _: TwoPairs => 1
		case _: Three => 1
		case that: Straight => this.highRank compare that.highRank
		case _ => -1
	}

	override lazy val toString: String = "Straight from " + highRank

	override lazy val normalizedPriority: Int = highRank.normalizedPriority

	override val minPriority: Int = Three.MaxPriority + 1

	override val prioritiesCount: Int = Straight.MaxPriority - minPriority + 1
}

object Straight{
	val MaxPriority: Int = Three.MaxPriority + Rank.values.size
}

case class Three(rank: HoldemRank, otherRanks: Seq[HoldemRank]) extends HoldemHand with PrioritiesSeq{
	require(rank != null)
	require(otherRanks != null)
	require(otherRanks.size == 2)
	otherRanks foreach (r => require(r != null))
	requireDifferent(rank, otherRanks(0), otherRanks(1))
	requireDescendingDistinct(otherRanks:_*)

	override def compare(that: HoldemHand): Int = that match{
		case null => 1
		case _: HighCard => 1
		case _: Pair => 1
		case _: TwoPairs => 1
		case that: Three =>
			val mainRanksResult = comparePairs(this.rank, that.rank)
			if(mainRanksResult == 0) compareLexicographically(this.otherRanks, that.otherRanks) else mainRanksResult
		case _ => -1
	}

	override lazy val toString: String = s"Three of $rank with " + otherRanks.mkString(" ")

	override val minPriority: Int = TwoPairs.MaxPriority + 1

	override protected lazy val priorities: Seq[Priority] = rank +: otherRanks
}

object Three{
	val MaxPriority: Int = TwoPairs.MaxPriority + (Rank.values.size pow 3).toInt

	def apply(rank: HoldemRank, otherRank1: HoldemRank, otherRank2: HoldemRank): Three = Three(rank, Seq(otherRank1, otherRank2))
}

case class TwoPairs(highRank: HoldemRank, lowRank: HoldemRank, otherRank: HoldemRank) extends HoldemHand with PrioritiesSeq{
	require(highRank != null)
	require(lowRank != null)
	require(otherRank != null)
	require(highRank > lowRank)
	requireDifferent(highRank, lowRank, otherRank)

	override def compare(that: HoldemHand): Int = that match{
		case null => 1
		case _: HighCard => 1
		case _: Pair => 1
		case that: TwoPairs => comparePairs(this.highRank, that.highRank, this.lowRank, that.lowRank, this.otherRank, that.otherRank)
		case _ => -1
	}

	override lazy val toString: String = s"Two pairs $highRank $lowRank with $otherRank"

	override val minPriority: Int = Pair.MaxPriority + 1

	override protected lazy val priorities: Seq[Priority] = List(highRank, lowRank, otherRank)
}

object TwoPairs{
	val MaxPriority: Int = Pair.MaxPriority + (Rank.values.size pow 3).toInt
}

case class Pair(rank: HoldemRank, otherRanks: Seq[HoldemRank]) extends HoldemHand with PrioritiesSeq{
	require(rank != null)
	require(otherRanks != null)
	require(otherRanks.size == 3)
	otherRanks foreach (r => require(r != null))
	requireDifferent(rank, otherRanks(0), otherRanks(1), otherRanks(2))
	requireDescendingDistinct(otherRanks:_*)

	override def compare(that: HoldemHand): Int = that match{
		case null => 1
		case _: HighCard => 1
		case that: Pair =>
			val mainRanksResult = this.rank compare that.rank
			if(mainRanksResult == 0) compareLexicographically(this.otherRanks, that.otherRanks) else mainRanksResult
		case _ => -1
	}

	override lazy val toString: String = s"Pair of $rank with " + otherRanks.mkString(" ")

	override val minPriority: Int = HighCard.MaxPriority + 1

	override protected lazy val priorities: Seq[Priority] = rank +: otherRanks
}

object Pair{
	val MaxPriority: Int = HighCard.MaxPriority + (Rank.values.size pow 4).toInt

	def apply(rank: HoldemRank, otherRank1: HoldemRank, otherRank2: HoldemRank, otherRank3: HoldemRank): Pair = Pair(rank, Seq(otherRank1, otherRank2, otherRank3))
}

case class HighCard(ranks: Seq[HoldemRank]) extends HoldemHand with PrioritiesSeq{
	require(ranks != null)
	require(ranks.size == 5)
	ranks foreach (r => require(r != null))
	requireDescendingDistinct(ranks:_*)
	require(ranks(0) - 4 != ranks(4), """It's not a "high card", but a straight""")

	override def compare(that: HoldemHand): Int = that match {
		case null => 1
		case that: HighCard => compareLexicographically(this.ranks, that.ranks)
		case _ => -1
	}

	override lazy val toString: String = "High card " + ranks.mkString(" ")

	override protected val priorities: Seq[Priority] = ranks
}

object HighCard{
	val MaxPriority: Int = (Rank.values.size pow 5).toInt - 1

	def apply(r1: HoldemRank, r2: HoldemRank, r3: HoldemRank, r4: HoldemRank, r5: HoldemRank): HighCard = HighCard(Seq(r1, r2, r3, r4, r5))
}