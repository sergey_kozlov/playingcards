package org.bitbucket.sergey_kozlov.playingcards.holdem

/**
 * User: Sergey Kozlov skozlov@poidem.ru
 * Date: 12.08.2014
 * Time: 10:38
 */

case class Position(playersAfter: Int){
	require(playersAfter >= 0)
}