package org.bitbucket.sergey_kozlov.playingcards.holdem.pocket

import org.bitbucket.sergey_kozlov.playingcards.holdem.HoldemRank

/**
 * User: Sergey Kozlov skozlov@poidem.ru
 * Date: 12.08.2014
 * Time: 11:34
 */

object HighPair{
	def unapply(cards: PocketCards): Option[HoldemRank] = if(cards == null || !cards.highPair) None else Some(cards.high.holdemRank)
}