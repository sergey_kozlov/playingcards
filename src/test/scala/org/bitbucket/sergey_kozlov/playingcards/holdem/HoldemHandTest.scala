package org.bitbucket.sergey_kozlov.playingcards.holdem

import junit.framework.TestCase
import org.junit.Test
import org.scalatest.MustMatchers
import org.bitbucket.sergey_kozlov.playingcards.core.Suit._
import org.bitbucket.sergey_kozlov.playingcards.deck52.Rank._
import org.bitbucket.sergey_kozlov.playingcards.holdem.HoldemRank.rank52ToHoldemRank
import org.bitbucket.sergey_kozlov.commons.constraint._

/**
 * User: Sergey Kozlov skozlov@poidem.ru
 * Date: 01.07.2014
 * Time: 9:30
 */

class HoldemHandTest extends TestCase with MustMatchers{
	@Test
	def testApplyRoyalFlush(){
		HoldemHand(Set(HoldemCard(K, Hearts), HoldemCard(_9, Hearts), HoldemCard(_9, Clubs), HoldemCard(J, Hearts), HoldemCard(Q, Hearts), HoldemCard(A, Hearts), HoldemCard(T, Hearts))) match {
			case RoyalFlush =>
		}
	}

	@Test
	def testApplyStraightFlushNotWheel(){
		HoldemHand(Set(HoldemCard(A, Hearts), HoldemCard(_2, Hearts), HoldemCard(_7, Clubs), HoldemCard(_6, Hearts), HoldemCard(_5, Hearts), HoldemCard(_4, Hearts), HoldemCard(_3, Hearts))) match {
			case StraightFlush(highRank) => highRank mustBe rank52ToHoldemRank(_6)
		}
	}

	@Test
	def testApplyStraightFlushWheel(){
		HoldemHand(Set(HoldemCard(A, Hearts), HoldemCard(_2, Hearts), HoldemCard(K, Hearts), HoldemCard(_6, Clubs), HoldemCard(_5, Hearts), HoldemCard(_4, Hearts), HoldemCard(_3, Hearts))) match {
			case StraightFlush(highRank) => highRank mustBe rank52ToHoldemRank(_5)
		}
	}

	@Test
	def testApplyQuads(){
		HoldemHand(Set(HoldemCard(T, Hearts), HoldemCard(A, Hearts), HoldemCard(T, Diamonds), HoldemCard(K, Diamonds), HoldemCard(T, Clubs), HoldemCard(K, Clubs), HoldemCard(T, Spades))) match {
			case Quads(rank, otherRank) =>
				rank mustBe rank52ToHoldemRank(T)
				otherRank mustBe rank52ToHoldemRank(A)
		}
	}
	
	@Test
	def testApplyFullHouse1Three(){
		HoldemHand(Set(HoldemCard(Q, Hearts), HoldemCard(Q, Clubs), HoldemCard(K, Hearts), HoldemCard(A, Hearts), HoldemCard(K, Diamonds), HoldemCard(A, Diamonds), HoldemCard(K, Clubs))) match {
			case FullHouse(threeRank, pairRank) =>
				threeRank mustBe rank52ToHoldemRank(K)
				pairRank mustBe rank52ToHoldemRank(A)
		}
	}

	@Test
	def testApplyFullHouse2Threes(){
		HoldemHand(Set(HoldemCard(A, Hearts), HoldemCard(Q, Hearts), HoldemCard(K, Hearts), HoldemCard(Q, Diamonds), HoldemCard(K, Diamonds), HoldemCard(Q, Clubs), HoldemCard(K, Clubs))) match {
			case FullHouse(threeRank, pairRank) =>
				threeRank mustBe rank52ToHoldemRank(K)
				pairRank mustBe rank52ToHoldemRank(Q)
		}
	}

	@Test
	def testApplyFlush(){
		HoldemHand(Set(HoldemCard(A, Diamonds), HoldemCard(K, Hearts), HoldemCard(Q, Hearts), HoldemCard(_2, Hearts), HoldemCard(_3, Hearts), HoldemCard(J, Hearts), HoldemCard(T, Hearts))) match {
			case Flush(ranks) => ranks mustBe (Seq(K, Q, J, T, _3) map rank52ToHoldemRank)
		}
	}

	@Test
	def testApplyStraightNotWheel(){
		HoldemHand(Set(HoldemCard(A, Hearts), HoldemCard(_2, Diamonds), HoldemCard(A, Clubs), HoldemCard(_6, Spades), HoldemCard(_5, Hearts), HoldemCard(_4, Diamonds), HoldemCard(_3, Clubs))) match {
			case Straight(highRank) => highRank mustBe rank52ToHoldemRank(_6)
		}
	}

	@Test
	def testApplyStraightWheel(){
		HoldemHand(Set(HoldemCard(A, Hearts), HoldemCard(_2, Diamonds), HoldemCard(A, Clubs), HoldemCard(A, Spades), HoldemCard(_5, Hearts), HoldemCard(_4, Diamonds), HoldemCard(_3, Clubs))) match {
			case Straight(highRank) => highRank mustBe rank52ToHoldemRank(_5)
		}
	}

	@Test
	def testApplyThree(){
		HoldemHand(Set(HoldemCard(_9, Spades), HoldemCard(Q, Hearts), HoldemCard(T, Hearts), HoldemCard(K, Diamonds), HoldemCard(T, Diamonds), HoldemCard(A, Clubs), HoldemCard(T, Clubs))) match {
			case Three(rank, otherRanks) =>
				rank mustBe rank52ToHoldemRank(T)
				otherRanks mustBe (Seq(A, K) map rank52ToHoldemRank)
		}
	}

	@Test
	def testApplyTwoPairs(){
		HoldemHand(Set(HoldemCard(J, Hearts), HoldemCard(J, Diamonds), HoldemCard(Q, Hearts), HoldemCard(A, Hearts), HoldemCard(Q, Diamonds), HoldemCard(K, Diamonds), HoldemCard(A, Diamonds))) match {
			case TwoPairs(highRank, lowRank, otherRank) =>
				highRank mustBe rank52ToHoldemRank(A)
				lowRank mustBe rank52ToHoldemRank(Q)
				otherRank mustBe rank52ToHoldemRank(K)
		}
	}

	@Test
	def testApplyPair(){
		HoldemHand(Set(HoldemCard(_2, Hearts), HoldemCard(_8, Clubs), HoldemCard(Q, Hearts), HoldemCard(K, Hearts), HoldemCard(A, Hearts), HoldemCard(_2, Clubs))) match {
			case Pair(rank, otherRanks) =>
				rank mustBe rank52ToHoldemRank(_2)
				otherRanks mustBe (Seq(A, K, Q) map rank52ToHoldemRank)
		}
	}

	@Test
	def testApplyHighCard(){
		HoldemHand(Set(HoldemCard(_8, Clubs), HoldemCard(_9, Clubs), HoldemCard(K, Hearts), HoldemCard(A, Hearts), HoldemCard(Q, Hearts), HoldemCard(J, Hearts))) match{
			case HighCard(ranks) =>
				ranks mustBe (Seq(A, K, Q, J, _9) map rank52ToHoldemRank)
		}
	}

	@Test
	def testCompare(){
		requireDescendingDistinct(
			RoyalFlush,
			StraightFlush(K), StraightFlush(_5),
			Quads(A, K), Quads(A, _2), Quads(K, _3), Quads(_2, _3),
			FullHouse(A, K), FullHouse(A, _2), FullHouse(K, _3), FullHouse(_2, _3),
			Flush(A, K, Q, J, _9), Flush(A, K, Q, J, _2), Flush(A, K, Q, _4, _3), Flush(A, K, _6, _5, _4), Flush(A, _8, _7, _6, _5), Flush(K, Q, J, T, _8), Flush(_7, _5, _4, _3, _2),
			Straight(A), Straight(_5),
			Three(A, K, Q), Three(A, K, _2), Three(A, _4, _3), Three(K, A, Q), Three(_2, _4, _3),
			TwoPairs(A, K, Q), TwoPairs(A, K, _2), TwoPairs(A, _2, _3), TwoPairs(K, Q, A), TwoPairs(_3, _2, _4),
			Pair(A, K, Q, J), Pair(A, K, Q, _2), Pair(A, K, _4, _3), Pair(A, _6, _5, _4), Pair(K, Q, J, T), Pair(_2, _5, _4, _3),
			HighCard(A, K, Q, J, _9), HighCard(A, K, Q, J, _2), HighCard(A, K, Q, _4, _3), HighCard(A, K, _6, _5, _4), HighCard(A, _8, _7, _6, _5), HighCard(K, Q, J, T, _8), HighCard(_7, _5, _4, _3, _2))
	}

	@Test
	def testHighCardMaxPriority(){
		HighCard(A, K, Q, J, _9).maxPriority mustBe HighCard.MaxPriority
	}

	@Test
	def testPairMaxPriority(){
		Pair(A, K, Q, J).maxPriority mustBe Pair.MaxPriority
	}

	@Test
	def testTwoPairsMaxPriority(){
		TwoPairs(A, K, Q).maxPriority mustBe TwoPairs.MaxPriority
	}

	@Test
	def testThreeMaxPriority(){
		Three(A, K, Q).maxPriority mustBe Three.MaxPriority
	}

	@Test
	def testStraightMaxPriority(){
		Straight(A).maxPriority mustBe Straight.MaxPriority
	}

	@Test
	def testFlushMaxPriority(){
		Flush(A, K, Q, J, _9).maxPriority mustBe Flush.MaxPriority
	}

	@Test
	def testFullHouseMaxPriority(){
		FullHouse(A, K).maxPriority mustBe FullHouse.MaxPriority
	}

	@Test
	def testQuadsMaxPriority(){
		Quads(A, K).maxPriority mustBe Quads.MaxPriority
	}

	@Test
	def testStraightFlushMaxPriority(){
		StraightFlush(K).maxPriority mustBe StraightFlush.MaxPriority
	}
}