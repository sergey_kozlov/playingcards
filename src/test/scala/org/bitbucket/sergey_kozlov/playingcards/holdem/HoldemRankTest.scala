package org.bitbucket.sergey_kozlov.playingcards.holdem

import junit.framework.TestCase
import org.junit.Test
import org.scalatest.MustMatchers
import org.bitbucket.sergey_kozlov.playingcards.deck52.Rank._
import org.bitbucket.sergey_kozlov.playingcards.holdem.HoldemRank.rank52ToHoldemRank

/**
 * User: Sergey Kozlov skozlov@poidem.ru
 * Date: 30.06.2014
 * Time: 10:35
 */

class HoldemRankTest extends TestCase with MustMatchers{
	@Test
	def testCompareEqual(){
		(rank52ToHoldemRank(A) compare rank52ToHoldemRank(A)) mustBe 0
	}

	@Test
	def testCompareDifferent(){
		assert(rank52ToHoldemRank(A) > rank52ToHoldemRank(K))
	}

	@Test
	def testPlusNoOverflow(){
		(K + 1) mustBe rank52ToHoldemRank(A)
	}

	@Test
	def testPlusOverflow(){
		(A + 1) mustBe rank52ToHoldemRank(_2)
	}

	@Test
	def testMinus(){
		(A - 1) mustBe rank52ToHoldemRank(K)
	}
}