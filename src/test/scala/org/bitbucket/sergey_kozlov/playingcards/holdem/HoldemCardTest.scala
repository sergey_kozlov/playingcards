package org.bitbucket.sergey_kozlov.playingcards.holdem

import junit.framework.TestCase
import org.junit.Test
import org.scalatest.MustMatchers
import org.bitbucket.sergey_kozlov.playingcards.deck52.Rank._
import org.bitbucket.sergey_kozlov.playingcards.core.Suit._
import HoldemRank.rank52ToHoldemRank

/**
 * User: Sergey Kozlov skozlov@poidem.ru
 * Date: 30.06.2014
 * Time: 10:54
 */

class HoldemCardTest extends TestCase with MustMatchers{
	@Test
	def testCompareEqualRanks(){
		(HoldemCard(A, Hearts) compare HoldemCard(A, Clubs)) mustBe 0
	}

	@Test
	def testCompareDifferentRanks(){
		HoldemCard(A, Hearts) mustBe > (HoldemCard(K, Hearts))
	}
}