organization := "org.bitbucket.sergey_kozlov"

name := "playingcards"

version := "0.1-SNAPSHOT"

publishMavenStyle := true

publishTo := Some(Resolver.file("file",  new File(Path.userHome.absolutePath+"/.m2/repository")))

publishArtifact in Test := false

pomIncludeRepository := { _ => false }

pomExtra :=
		<url>https://bitbucket.org/sergey_kozlov/playingcards</url>
		<licenses>
			<license>
				<name>The MIT License</name>
				<url>http://www.opensource.org/licenses/mit-license.php</url>
				<distribution>repo</distribution>
			</license>
		</licenses>
		<scm>
			<url>https://bitbucket.org/sergey_kozlov/playingcards.git</url>
			<connection>scm:git:ssh://git@bitbucket.org/sergey_kozlov/playingcards.git</connection>
		</scm>
		<developers>
			<developer>
				<id>skozlov</id>
				<name>Sergey Kozlov</name>
				<email>mail.sergey.kozlov@gmail.com</email>
				<roles>
					<role>architect</role>
					<role>developer</role>
				</roles>
			</developer>
		</developers>

libraryDependencies ++= Seq(
	"junit" % "junit" % "4.11",
	"org.scalatest" % "scalatest_2.10" % "2.0" % "test",
	"org.bitbucket.sergey_kozlov" % "commons_2.10" % "0.1-SNAPSHOT",
	"org.bitbucket.sergey_kozlov" % "math_2.10" % "0.1-SNAPSHOT"
)